# Internet of Things - 5th Year Informatics Coursework #

The goal of this group coursework was to build a full indoor positioning system, which is based on Bluetooth Low Energy beacon readings. In addition to the required hardware, the only provided resource was a dedicated cloud service for data storage and processing.

The three resulting folders each contain all the code for each dedicated part of the system:

* firmware: C code to be run on a Nordic nRF51 board, responsible for:
	* Gathering and pre-processing Bluetooth beacon data
	* Sending beacon data to an Android phone, via Bluetooth
* app: Java code to be run on an Android phone, responsible for:
	* Receiving beacon data from the nRF51 board and further pre-processing it
	* Storing beacon data in the cloud service, via REST-based HTTP
	* Polling the cloud database for the final position data
	* Providing real-time visualisation for the current position, using the Google Maps API
* cloud-scripts: Python code running on the cloud service, responsible for:
	* Receiving beacon data from the Android phone
	* Using two separate algorithms to estimate the position of the nRF51 board
	* Storing all intermediate and final data in a MongoDB database
* cloud-web: HTML/JS/CSS code running on the cloud service, responsible for:
	* Polling the cloud database for the final position data
	* Providing real-time visualisation for the current position, using the Google Maps API