// Variable implementing base HTTP POST functionality
var HttpClient = function() {
	var secret = "Bearer 59:f1a0e0e20d7ba1484533a7833a66f7849805e51a";
	this.get = function(url, callback) {

		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() { 
			if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200)
				callback(xhr.responseText);
		}

		xhr.open("GET", url, true);
		xhr.setRequestHeader("Authorization", secret);
		xhr.send(null);
	}

	this.post = function(url, data, callback) {

		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() { 
			if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200)
				callback(xhr.responseText);
		}

		xhr.open("POST", url, true);
		xhr.setRequestHeader("Authorization", secret);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.send(data);
	}
}

// Pull latest positions every 0.5s
function requestNewestPosition() {
	var timeout = 3;
	var curTime = (new Date().getTime())/1000;
	var query = '{"timestamp": {"$gt": ' + (curTime-timeout).toString() + '}}'
	var url = "http://glenlivet.inf.ed.ac.uk:8080/api/v1/svc/apps/data/docs/positions/query"
	var client = new HttpClient();
	client.post(url, query, function(response) {
		extractPosition(response, curTime);
	});
		
	setTimeout(requestNewestPosition, 500);
}

// Extract coordinates and accuracy from the very latest position
function extractPosition(data, curTime) {
	var jsonData = JSON.parse(data);
	
	if (jsonData.length>0) {	
		var lastOne = jsonData[jsonData.length-1];
		var coordsList = JSON.parse(lastOne['pos']);
		var acc = lastOne['acc'];
		moveMarker(coordsList[0], coordsList[1], acc);
	}
}

// Pull all measurements and pass them to the CSV assembly function
function requestAsCSV(flag) {
	var url = "http://glenlivet.inf.ed.ac.uk:8080/api/v1/svc/apps/data/docs/positions"
	var client = new HttpClient();
	client.get(url, function(response) {
		assembleCSV(flag, response);
	});
}

// Assemble and download all data as CSV
function assembleCSV(flag, data) {
	var jsonData = JSON.parse(data);
	if (jsonData.length>0) {
		var i;
		var csv = "";
		for (i=0; i<jsonData.length; i++) {
			var coordsList = JSON.parse(jsonData[i]['pos']);
			var timestamp = jsonData[i]['timestamp'];
			if (flag==0) {
				csv += timestamp + "," + coordsList[0] + "," + coordsList[1] + "\n";
			} else {
				var acc = jsonData[i]['acc'];
				csv += timestamp + "," + coordsList[0] + "," + coordsList[1] + "," + acc + "\n";
			}
		}
		var csvData = new Blob([csv]);
		var csvElement = document.getElementById("csv");
		csvElement.href = URL.createObjectURL(csvData);
		csvElement.click();
	}
}





