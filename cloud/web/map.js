var marker;

// Initiate the Google Map, marker and overlay
function initMap() {
	var latLng = {lat: 55.94443, lng: -3.18675}

	var map = new google.maps.Map(document.getElementById('map'), {
		center: latLng,
		zoom: 20
	});

	marker = new google.maps.Marker({
		map: map,
		position: latLng
	});

	var imageBounds = {
		north: 55.944597,
		south: 55.944264,
		east: -3.186329,
		west: -3.187167
	};

	var overlay = new google.maps.GroundOverlay(
		'http://glenlivet.inf.ed.ac.uk:8090/59/floor.png',
		imageBounds);

	overlay.setMap(map);
}

// Move marker to specified position, change its colour according to accuracy
function moveMarker(lat, lon, acc) {
	switch (acc) {
		case "low":
			marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
			break;
		case "medium":
			marker.setIcon('http://maps.google.com/mapfiles/ms/icons/yellow-dot.png');
			break;
		default:
			marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
	}
	marker.setPosition( new google.maps.LatLng(lat, lon) );
}
