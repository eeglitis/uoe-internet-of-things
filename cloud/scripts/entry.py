#!/usr/bin/python

'''
Main script initiated by the app. Specifies the highest-level overview
of all scripting operations.
'''

import cloud
import filtering
import compute

if __name__ == "__main__":

	baseBeaconInfo, groundPoints = cloud.getBaseData()
	beaconInfo = filtering.formatBeaconInfo(baseBeaconInfo)

	while (cloud.checkShutdownFlag()==[]):
		baseBeaconMeasurements = cloud.getBeaconMeasurements(beaconInfo)

		if (len(baseBeaconMeasurements)>0):
			timestamp = filtering.extractLatestTime(baseBeaconMeasurements)
			beaconMeasurements = filtering.formatBeaconMeasurements(baseBeaconMeasurements)

			bestGroundPoints = compute.findBestGroundPoints(groundPoints, beaconMeasurements)

			if (len(beaconMeasurements)>=3):
				position = compute.wclEnhancedFingerprinting(beaconInfo, beaconMeasurements, bestGroundPoints)
				cloud.storeLocation(position, timestamp, "high")
			elif (len(beaconMeasurements)==2):
				position = compute.standardFingerprinting(bestGroundPoints)
				cloud.storeLocation(position, timestamp, "medium")
			else:
				position = compute.standardFingerprinting(bestGroundPoints)
				cloud.storeLocation(position, timestamp, "low")

