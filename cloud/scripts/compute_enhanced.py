#!/usr/bin/python

'''
Computation methods used in the enhanced position estimation technique.
'''

import ast
import compute_math as cm

# Take the ground point coordinates and characteristic distances,
# and extract just their coordinates
def extractGroundPointCoords(groundPoints):
	coordsList = []
	for point in groundPoints:
		groundPointCoords = ast.literal_eval(point[0])
		coordsList.append(groundPointCoords)
	return coordsList

# Extract nearest/strongest beacon coordinates and calculate their distances
def findBestBeacons(beaconInfo, beaconMeasurements, n):
	beaconsFound = len(beaconMeasurements)
	if (beaconsFound>3):
		beaconMeasurements = filterBeacons(beaconMeasurements)

	beaconList = list(beaconMeasurements)	# Convert to list to ensure same order

	beaconPositions = []
	beaconDistances = []
	for beacon in beaconList:
		[position, meterSignal] = beaconInfo[beacon]
		actualSignal = beaconMeasurements[beacon]
		distance = cm.beaconDistanceFromRSSI(meterSignal, actualSignal, n)
		beaconPositions.append(position)
		beaconDistances.append(distance)

	return beaconPositions, beaconDistances

# If 3+ beacons in range, use only 3 strongest
def filterBeacons(beaconMeasurements):
	sortedByFurthest = sorted(beaconMeasurements.items(), key=lambda x: x[1])
	bestBeaconMeasurements = dict(sortedByFurthest[-3:])

	return bestBeaconMeasurements
	
# Calculate actual distances from ground points to estimated position
def calculateDistances(groundTruths, estimatedPosition):
	distances = []
	for groundPoint in groundTruths:
		groundPointCoords = ast.literal_eval(groundPoint[0])
		distance = cm.distanceBetweenTwoPoints(estimatedPosition, groundPointCoords)
		distances.append(distance)

	return distances











