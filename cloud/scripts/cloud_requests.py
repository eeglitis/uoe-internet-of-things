#!/usr/bin/python

'''
Lower-level HTTP request methods.
'''

from urllib.request import Request, urlopen

secret = "Bearer 59:f1a0e0e20d7ba1484533a7833a66f7849805e51a"

# GET request
def get(url):
	req = Request(url)
	req.add_header('Authorization', secret)
	resp = urlopen(req).read()
	return resp.decode("utf-8")

# POST request (given JSON input data)
def post(url, data):
	req = Request(url, data.encode("utf-8"))
	req.add_header('Authorization', secret)
	req.add_header('Content-Type', 'application/json')
	resp = urlopen(req).read()
	return resp.decode("utf-8")
