#!/usr/bin/python

'''
Core math methods used in position estimation calculations.
'''

import math

earthR = 6381*1000

# Calculates distance (in m) given two lat/lon point coordinates
# Simplified according to Wikipedia
def distanceBetweenTwoPoints(p1, p2):
	[lat1, lon1] = p1
	[lat2, lon2] = p2

	latDistance = math.radians(lat2 - lat1);
	lonDistance = math.radians(lon2 - lon1);
	a = math.sin(latDistance / 2)**2 + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * (math.sin(lonDistance / 2)**2);
	distance = 2 * earthR * math.asin(math.sqrt(a))

	return distance

# Estimate distance from single beacon given its output power and RSSI
# Adapted from Subedi and Pyun, 2017
def beaconDistanceFromRSSI(power1m, rssi, n):
	ratio_db = power1m - rssi
	distance = pow(10, ratio_db / (10*n))

	return distance

# Weighted Centroid Localisation
# Adapted from Subedi and Pyun, 2017
def wcl(points, distances, g):
	weights = [1/(distances[0]**g), 1/(distances[1]**g), 1/(distances[2]**g)]
	xs = [points[0][1], points[1][1], points[2][1]] # longitude
	ys = [points[0][0], points[1][0], points[2][0]] # latitude
	sum_w = sum(weights)
	xw = (xs[0]*weights[0] + xs[1]*weights[1] + xs[2]*weights[2])/sum_w
	yw = (ys[0]*weights[0] + ys[1]*weights[1] + ys[2]*weights[2])/sum_w

	return [yw, xw]


