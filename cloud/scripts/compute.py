#!/usr/bin/python

'''
Computation methods accessed through the main script file.
'''

import ast
import math
import compute_enhanced as ce
import compute_math as cm

# Find the 3 nearest neighbor ground points to the measurements
def findBestGroundPoints(groundPoints, beaconMeasurements):
	positioningDistances = {}

	for groundPoint in groundPoints:
		groundPointReadings = ast.literal_eval(groundPoint['beacons'])
		cumulativeDistance = 0

		for beacon in beaconMeasurements.keys():
			if beacon in groundPointReadings:
				cumulativeDistance += (beaconMeasurements[beacon] - groundPointReadings[beacon])**2
			else:
				cumulativeDistance += (beaconMeasurements[beacon] + 104)**2			# Beacon not seen, use default value of -104

		# Catch edge case where all measurements match those at ground point
		# More likely to happen if only one beacon is found
		if (cumulativeDistance==0):
			cumulativeDistance = 0.01

		positioningDistances[groundPoint['pos']] = math.sqrt(cumulativeDistance)

	distancesSortedByFurthest = sorted(positioningDistances.items(), key=lambda x: x[1])
	bestGroundPoints = distancesSortedByFurthest[:3]

	return bestGroundPoints

# Measurements contain at least 3 beacon signals: apply enhanced technique
# Adapted from Subedi and Pyun, 2017
def wclEnhancedFingerprinting(beaconInfo, beaconMeasurements, bestGroundPoints, n=1.5, g=0.5):
	bestGroundPointCoords = ce.extractGroundPointCoords(bestGroundPoints)

	beaconPositions, beaconDistances = ce.findBestBeacons(beaconInfo, beaconMeasurements, n)
	estimatedPosition = cm.wcl(beaconPositions, beaconDistances, g)
	distancesToBestGround = ce.calculateDistances(bestGroundPoints, estimatedPosition)
	finalPosition = cm.wcl(bestGroundPointCoords, distancesToBestGround, g)

	return finalPosition

# Measurements contain less than 3 beacon signals: apply simple technique
def standardFingerprinting(bestGroundPoints, g=0.5):
	groundPointCoords = []
	groundPointDistances = []
	for groundPoint in bestGroundPoints:
		coords = ast.literal_eval(groundPoint[0])
		groundPointCoords.append(coords)
		groundPointDistances.append(groundPoint[1])
		
	position = cm.wcl(groundPointCoords, groundPointDistances, g)

	return position
