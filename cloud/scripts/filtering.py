#!/usr/bin/python

'''
Methods for transforming data, mainly to filter and convert
the data pulled from the cloud.
'''

import statistics

# Format base beacon info (discard mongodb ID and index by MAC)
def formatBeaconInfo(baseBeaconInfo):
	formattedBeaconInfo = {}
	for beacon in baseBeaconInfo:
		formattedBeaconInfo[beacon['mac']] = [beacon['loc'], beacon['meter']]

	return formattedBeaconInfo

# Get the most recent timestamp from received beacon data
def extractLatestTime(beaconMeasurements):
	latestTime = 0
	for beacon in beaconMeasurements:
		for entry in beaconMeasurements[beacon]:
			entryTime = entry['timestamp']
			if (entryTime>latestTime):
				latestTime = entryTime

	return latestTime

# Format received beacon data (replace each beacon value with its average signal)
def formatBeaconMeasurements(beaconMeasurements):
	for beacon in beaconMeasurements:
		for entryIndex in range(len(beaconMeasurements[beacon])):
			beaconMeasurements[beacon][entryIndex] = beaconMeasurements[beacon][entryIndex]['signal']
		beaconMeasurements[beacon] = statistics.mean(beaconMeasurements[beacon])

	return beaconMeasurements


