#!/usr/bin/python

'''
Higher-level methods for interacting with the cloud.
'''

import json
import time
import cloud_requests as cr

containers = "http://glenlivet.inf.ed.ac.uk:8080/api/v1/svc/apps/data/docs"

# Retrieve basic beacon info and stored ground truth
def getBaseData():
	beaconInfoString = cr.get(containers + "/beacon-info")
	beaconInfo = json.loads(beaconInfoString)

	groundPointsString = cr.get(containers + "/groundtruths")
	groundPoints = json.loads(groundPointsString)

	return beaconInfo, groundPoints

# Retrieve latest measurements from a single beacon
def getSingleBeaconMeasurements(mac, queryForm):
	beaconMeasurementsString = cr.post(containers + "/" + mac + "/query", queryForm)	
	beaconMeasurements = json.loads(beaconMeasurementsString)

	return beaconMeasurements

# Retrieve latest measurements from all beacons (within timeout period)
def getBeaconMeasurements(beaconInfo, timeout=10):
	timeCutoff = round(time.time()) - timeout
	queryForm = '{"timestamp": {"$gt": ' + str(timeCutoff) + '}}'

	beaconMeasurements = {}
	for mac in beaconInfo.keys():
		singleBeaconMeasurements = getSingleBeaconMeasurements(mac, queryForm)
		if (singleBeaconMeasurements!=[]):
			beaconMeasurements[mac] = singleBeaconMeasurements

	return beaconMeasurements

# Store final position and time in the cloud
def storeLocation(position, timestamp, accuracy):
	inputData = '{"timestamp": ' + str(timestamp) + ', "pos": "' + str(position) + '", "acc": "' + accuracy + '"}'
	cr.post(containers + "/positions", inputData)

# Check if the shutdown container has data, indicating disconnection
def checkShutdownFlag():
	resp = cr.get(containers + "/shutdown")
	result = json.loads(resp)

	return result
