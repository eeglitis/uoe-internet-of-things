package com.iotssc.potatomap;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.util.Log;

import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;


public class GattRequestQueue {


    private static final UUID BLE_CLIENT_CHARACTERISTIC_CONFIG_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private static final String TAG = "GattRequestQueue";

    private Queue<GattCharacteristicRequest> requestQueue;
    private boolean verbose = false;
    private boolean paused = false;
    private BluetoothGatt gatt;


    public GattRequestQueue(BluetoothGatt gatt) {
        this.gatt = gatt;
        requestQueue = new LinkedList<>();
    }

    public enum GattAction {
        READ,
        ENABLE_NOTIFICATION
    }

    ;


    private class GattCharacteristicRequest {

        BluetoothGattCharacteristic characteristic;
        GattAction action;

        public GattCharacteristicRequest(BluetoothGattCharacteristic characteristic, GattAction action) {
            if (characteristic == null || action == null) {
                Log.e(TAG, "Attempt to create GattCharacteristicRequest with NULL parameter");
                return;
            }
            this.characteristic = characteristic;
            this.action = action;
        }
    }

    public synchronized void pauseQueue() {
        this.paused = true;
    }

    public synchronized void resumeQueue() {
        this.paused = false;
        //now that is was unpaused, start stuff
        startFront();
    }

    public int getSize() {
        return requestQueue.size();
    }

    public synchronized void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        queueGattRequest(characteristic, GattAction.READ);
    }

    public synchronized void enableCharacteristicNotifications(BluetoothGattCharacteristic characteristic) {
        queueGattRequest(characteristic, GattAction.ENABLE_NOTIFICATION);
    }


    public synchronized void sendNext() {
        if (requestQueue.isEmpty()) {
            return;
        }
        //remove queue head
        requestQueue.remove();
        //start the queue head, if such remains
        startFront();
    }

    public synchronized void resendCurrent()
    {
        startFront();
    }

    //must be exclusive access
    private void queueGattRequest(BluetoothGattCharacteristic characteristic, GattAction action) {
        //only try reading a readable value
        int properties = characteristic.getProperties();
        //iof not readable - exit

        if (((properties & BluetoothGattCharacteristic.PROPERTY_READ) == 0) && (action == GattAction.READ)) {
            if (verbose) {
                Log.e(TAG, "Cannot read a non-readable gatt char " + characteristic.getUuid().toString()
                        + "\r\n of service " + characteristic.getService().getUuid().toString());
            }
            return;
        } else if (((properties & BluetoothGattCharacteristic.PROPERTY_NOTIFY) == 0) && (action == GattAction.ENABLE_NOTIFICATION)) {
            if (verbose) {
                Log.e(TAG, "Cannot enable a notifications for a non-notifiable gatt char " + characteristic.getUuid().toString()
                        + "\r\n of service " + characteristic.getService().getUuid().toString());
            }
            return;
        }

        //add to tail
        requestQueue.add(new GattCharacteristicRequest(characteristic, action));

        //if it is the only request in the queue
        if (requestQueue.size() == 1) {
            //and start it
            startFront();
        }
    }

    ///start the next operation
    private void startFront() {
        if (paused) {
            if (verbose) {
                Log.e(TAG, "Cannot start operation:queue paused");
            }
            return;

        }
        //if no outstanding requests can start this one
        if (!requestQueue.isEmpty()) {

            GattCharacteristicRequest request = requestQueue.peek();
            BluetoothGattCharacteristic characteristic = request.characteristic;

            if (request.action == GattAction.READ) {

                gatt.readCharacteristic(characteristic);

                if (verbose) {
                    Log.e(TAG, "Started gatt char " + characteristic.getUuid().toString()
                            + " of service " + characteristic.getService().getUuid().toString()
                            + " read + \r\n" + "new queue size " + requestQueue.size());
                }
            }
            //otherwise it is a write
            else if (request.action == GattAction.ENABLE_NOTIFICATION) {

                BluetoothGattDescriptor descriptor = characteristic.getDescriptor(BLE_CLIENT_CHARACTERISTIC_CONFIG_UUID);
                if (descriptor == null) {
                    return;
                }

                gatt.setCharacteristicNotification(characteristic, true);
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                gatt.writeDescriptor(descriptor); //descriptor write operation successfully started?

                if (verbose) {
                    Log.e(TAG, "Enabled notification for service " + characteristic.getService().getUuid().toString()
                            + "\r\n for characteristic " + characteristic.getUuid().toString());
                }
            }
        }
    }
}


