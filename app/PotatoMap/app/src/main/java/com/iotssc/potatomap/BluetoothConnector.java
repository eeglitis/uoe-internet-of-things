package com.iotssc.potatomap;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;
import java.util.UUID;

/**
 * Created by Eugene on 15/04/2018.
 */


public class BluetoothConnector {

    public final String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    final private UUID BLE_IMU_SERVICE_UUID = UUID.fromString("00000002-0000-1000-8000-00805f9b34fb");
    final private UUID BLE_IMU_SERVICE_ACCEL_CHAR_UUID = UUID.fromString("00000001-0000-1000-8000-00805f9b34fb");
    final private UUID BLE_IMU_SERVICE_GYRO_CHAR_UUID = UUID.fromString("00000002-0000-1000-8000-00805f9b34fb");
    final private UUID BLE_IMU_SERVICE_MAGNET_CHAR_UUID = UUID.fromString("00000003-0000-1000-8000-00805f9b34fb");
    final int SCAN_PERIOD = 10000;
    final int REQUEST_ENABLE_BT = 1;
    final private int MAC_ADDRESS_SIZE = 6;
    final private int BLE_BEACON_COUNT = 10;
    final private LatLng BEACON_LOCATION_COORDINATES[] = {
            new LatLng(55.9444578385393, -3.1866151839494705),
            new LatLng(55.94444244275808, -3.18672649562358860),
            new LatLng(55.94452336441765, -3.1866540759801865),
            new LatLng(55.94452261340533, -3.1867526471614838),
            new LatLng(55.94448393625199, -3.1868280842900276),
            new LatLng(55.94449050761571, -3.1866483762860294),
            new LatLng(55.94443774892113, -3.1867992505431175),
            new LatLng(55.944432116316044, -3.186904862523079),
            new LatLng(55.94444938963575, -3.1869836524128914),
            new LatLng(55.94449107087541, -3.186941407620907)
    };
    final private String TAG = "BluetoothConnector";
    final private UUID BLE_BEACON_SERVICE_UUID = UUID.fromString("00000001-0000-1000-8000-00805f9b34fb");
    final private UUID BLE_BEACON_SERVICE_CHAR_UUIDs[] = {
            UUID.fromString("00000001-0000-1000-8000-00805f9b34fb"),
            UUID.fromString("00000002-0000-1000-8000-00805f9b34fb"),
            UUID.fromString("00000003-0000-1000-8000-00805f9b34fb"),
            UUID.fromString("00000004-0000-1000-8000-00805f9b34fb"),
            UUID.fromString("00000005-0000-1000-8000-00805f9b34fb"),
            UUID.fromString("00000006-0000-1000-8000-00805f9b34fb"),
            UUID.fromString("00000007-0000-1000-8000-00805f9b34fb"),
            UUID.fromString("00000008-0000-1000-8000-00805f9b34fb"),
            UUID.fromString("00000009-0000-1000-8000-00805f9b34fb"),
            UUID.fromString("0000000a-0000-1000-8000-00805f9b34fb")};
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private BluetoothGatt mGatt;

    private LatLng appletonCorners[] = {
            new LatLng(55.94438161058714f, -3.186342604458332f),
            new LatLng(55.9445943350147f, -3.1864482164382935f),
            new LatLng(55.944266892868924f, -3.18705003708601f),
            new LatLng(55.94447924242006f, -3.187159001827240f)};

    private Context context;
    private GattRequestQueue gattRequestQueue;
    private Handler mHandler;
    private boolean verboseDebug;
    private MainActivity activity;
    private BluetoothAdapter.LeScanCallback mScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice bluetoothDevice, int signalStrength, final byte[] scanRecord) {
            activity.recordBLEDevice(bluetoothDevice);
        }
    };
    private boolean BLEConnected = false;
    private boolean BLEdisconnectedDeliberately = false;
    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, final int newState) {

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                BLEConnected = true;
                Log.i(TAG, "Connected to GATT server.");
                if (mGatt.discoverServices()) {
                    Log.i(TAG, "Started service discovery.");
                } else {
                    Log.w(TAG, "Service discovery failed.");
                }
            } else {
                if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    //if were previously and did not deliberately disconnect
                    if (!BLEdisconnectedDeliberately && BLEConnected) {
                        try {
                            mGatt.connect();
                            Log.e(TAG, "BLE connection dropped, reconnecting");
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in reconnecting");
                        }
                    }
                    BLEdisconnectedDeliberately = false;
                    BLEConnected = false;
                }
            }
            activity.onConnectionStateChange(status, newState);
        }


        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {

            if (status == BluetoothGatt.GATT_SUCCESS) {

                List<BluetoothGattService> gattServices = mGatt.getServices();
                for (BluetoothGattService gattService : gattServices) {

                    List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
                    for (BluetoothGattCharacteristic characteristic : gattCharacteristics) {

                        if (characteristic.getService().getUuid().equals(BLE_IMU_SERVICE_UUID) ||
                                characteristic.getService().getUuid().equals(BLE_BEACON_SERVICE_UUID)) {

                            gattRequestQueue.enableCharacteristicNotifications(characteristic);
                            gattRequestQueue.readCharacteristic(characteristic);
                        }
                    }
                }

            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            interpretCharacteristicValue(characteristic);
        }


        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {


            if (status == BluetoothGatt.GATT_SUCCESS) {

                interpretCharacteristicValue(characteristic);
                gattRequestQueue.sendNext();
            }
            //if not a success, resend current request
            else {
                Log.e(TAG, "Service uuid: " + characteristic.getService().getUuid().toString() + "\r\n" +
                        "Characteristic uuid " + characteristic.getUuid().toString() + "\r\n" +
                        "read failed");

                gattRequestQueue.resendCurrent();
            }
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {

            if (status == BluetoothGatt.GATT_SUCCESS) {

                if (verboseDebug) {
                    Log.i(TAG, "Service uuid: " + descriptor.getCharacteristic().getService().getUuid().toString() + "\r\n" +
                            "Characteristic uuid " + descriptor.getCharacteristic().getUuid().toString() + "\r\n" +
                            "descriptor write successful");
                }

                //execute next request
                gattRequestQueue.sendNext();
            }
            //if not a success, resend current request
            else {
                Log.e(TAG, "Service uuid: " + descriptor.getCharacteristic().getService().getUuid().toString() + "\r\n" +
                        "Characteristic uuid " + descriptor.getCharacteristic().getUuid().toString() + "\r\n" +
                        "descriptor write failed");
                gattRequestQueue.resendCurrent();
            }
        }

    };

    BluetoothConnector(MainActivity activity, Context context, boolean verboseDebug) {
        this.context = context;
        this.verboseDebug = verboseDebug;
        this.activity = activity;
        mHandler = new Handler();
    }

    private void interpretCharacteristicValue(final BluetoothGattCharacteristic characteristic) {

        //if it is the beacon 8-bit dbms for tx power and rssi
        if (characteristic.getService().getUuid().equals(BLE_BEACON_SERVICE_UUID)) {

            for (int i = 0; i < BLE_BEACON_SERVICE_CHAR_UUIDs.length; i++) {

                if (characteristic.getUuid().equals(BLE_BEACON_SERVICE_CHAR_UUIDs[i])) {
                    byte rssiByte = characteristic.getValue()[0];
                    int rssiValue = intFromSignedByte(rssiByte);

                    if (verboseDebug) {

                        Log.e(TAG, "Beacon " + i + "\r\n" +
                                "RSSI : " + rssiValue);
                    }

                    MainActivity.networkQueries.postBeaconInfo(i, rssiValue);

                }


            }

        }
        //otherwise it is the floats from the IMU
        else if (characteristic.getService().getUuid().equals(BLE_IMU_SERVICE_UUID)) {

            byte bytes[] = characteristic.getValue();
            float floatValues[] = floatArrayFromByteArray(bytes);

            if (characteristic.getUuid().equals(BLE_IMU_SERVICE_ACCEL_CHAR_UUID)) {
                if (verboseDebug) {
                    Log.e(TAG, "Accelerometer new value \r\n" +
                            "X :" + floatValues[0] + "\r\n" +
                            "Y :" + floatValues[1] + "\r\n" +
                            "Z :" + floatValues[2]);
                }


            } else if (characteristic.getUuid().equals(BLE_IMU_SERVICE_GYRO_CHAR_UUID)) {
                if (verboseDebug) {
                    Log.e(TAG, "Gyroscope new value \r\n" +
                            "X :" + floatValues[0] + "\r\n" +
                            "Y :" + floatValues[1] + "\r\n" +
                            "Z :" + floatValues[2]);
                }

            } else if (characteristic.getUuid().equals(BLE_IMU_SERVICE_MAGNET_CHAR_UUID)) {
                if (verboseDebug) {
                    Log.e(TAG, "Magnetometer new value \r\n" +
                            "X :" + floatValues[0] + "\r\n" +
                            "Y :" + floatValues[1] + "\r\n" +
                            "Z :" + floatValues[2]);
                }
            }
        }
    }

    boolean isBluetoothAdapterReady() {
        return mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled();
    }

    void openConnection(BluetoothDevice device) {
        mGatt = device.connectGatt(context, true, mGattCallback);
        gattRequestQueue = new GattRequestQueue(mGatt);
    }

    void beginScanning() {
        if (!mScanning) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    finishScanning();
                }
            }, SCAN_PERIOD);

            mBluetoothAdapter.startLeScan(mScanCallback);
            mScanning = true;
        }
    }

    void finishScanning() {
        if (mScanning) {
            mBluetoothAdapter.stopLeScan(mScanCallback);
            mScanning = false;
        }
    }

    private int intFromSignedByte(byte byteValue) {
        int intValue = 0;
        //if the byte is a negative
        if ((byteValue & 0B10000000) > 0) {
            //then sign extend it
            intValue = -((~byteValue) + 1);
        } else {
            intValue = byteValue;
        }
        return intValue;
    }

    private float[] floatArrayFromByteArray(byte[] bytes) {
        //float is 4 bytes, in C++ and Java
        final int valueCount = bytes.length / 4;
        //dynamically create array

        FloatBuffer floatBuffer = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).asFloatBuffer();
        float floatValues[] = new float[floatBuffer.remaining()];
        floatBuffer.get(floatValues);

        return floatValues;
    }

    void configureBluetoothAdapter() {
        mBluetoothAdapter = ((BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
    }

    boolean isConnected() {
        return BLEConnected;
    }

    void disconnect() {
        //indicate disconnection was deliberate
        BLEdisconnectedDeliberately = true;
        mGatt.disconnect();
    }
}
