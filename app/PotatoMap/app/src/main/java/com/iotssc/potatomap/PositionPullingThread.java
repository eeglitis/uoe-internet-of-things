package com.iotssc.potatomap;

/**
 * PositionPullingThread.java
 * Continuously polls the cloud for the most recently computed positions.
 * Received data is passed to MainActivity.moveto().
 */

import android.os.SystemClock;
import android.util.Log;

import java.util.Date;

public class PositionPullingThread extends Thread {
    private String TAG = "PositionPullingThread";
    private int shutdownFlag = 0;
    private int timeout = 3;

    @Override
    public void run() {
        Log.i(TAG, "Pulling thread started");
        String resultContainer = "http://glenlivet.inf.ed.ac.uk:8080/api/v1/svc/apps/data/docs/positions/query";
        while (shutdownFlag==0) {
            long unixTime = new Date().getTime() / 1000;
            long cutoffTime = unixTime-timeout;
            String query = "{\"timestamp\": {\"$gt\": " +cutoffTime+ "}}";
            new NetworkConnection().execute("T", resultContainer, query);
            SystemClock.sleep(500);
        }
    }

    // Signal loop exit
    public void shutdown() {
        shutdownFlag = 1;
        Log.i(TAG, "Shutting down receiver");
    }
}
