package com.iotssc.potatomap;

/**
 * NetworkConnection.java
 * Implements basic HTTP POST functionality, including optional data retrieval.
 */

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkConnection extends AsyncTask<String,Void,String> {
    private String TAG = "NetworkConnection";
    private String returnFlag = "F";

    @Override
    protected String doInBackground(String... params) {
        Log.i(TAG, "New call for " + params[1]);
        String headerAuth = "Bearer 59:f1a0e0e20d7ba1484533a7833a66f7849805e51a";
        String result = "";
        HttpURLConnection urlConnection = null;
        returnFlag = params[0];
        try {
            URL url = new URL(params[1]);
            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestProperty("Authorization", headerAuth);
            urlConnection.setRequestProperty("Content-Type","application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);

            OutputStream os = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
            writer.write(params[2]);
            writer.flush();
            writer.close();
            os.close();

            urlConnection.connect();
            Log.i(TAG, "Sent data, response code " + urlConnection.getResponseCode());

            if (returnFlag.equals("T")) {
                InputStream in = urlConnection.getInputStream();
                result = getData(in);
                in.close();
            }

        } catch (IOException e) {
            Log.e(TAG, e.toString());

        } finally {
            urlConnection.disconnect();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        if (returnFlag.equals("T")) {
            result = result.trim();
            if (!result.equals("[]")) {
                MainActivity.moveTo(result);
            }
        }
    }

    // Extract received data from InputStream
    private String getData(InputStream stream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder result = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            reader.close();
        } catch (IOException e) {
            Log.e(TAG, e.toString());
        }
        return result.toString();
    }
}