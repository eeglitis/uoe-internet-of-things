package com.iotssc.potatomap;

/**
 * NetworkQueries.java
 * Implements various functions relating to cloud queries:
 * - Store new beacon readings
 * - Flag the cloud processing script to finish
 * - Clear the shutdown flag to allow further script launches
 */

import android.util.Log;

import java.util.Date;

public class NetworkQueries {
    private String TAG = "NetworkQueries";
    private String docs = "http://glenlivet.inf.ed.ac.uk:8080/api/v1/svc/apps/data/docs";

    private String[] macList = { "ed23c0d875cd",
            "e7311a8eb6d7", "c7bc919b2d17", "ec75a5ed8851",
            "fe12def2c943", "c03b5cfa00b8", "e0b83a2f802a",
            "f15576cb0cf8", "f17fb178ea3d", "fd8185988862"
    };

    // Formats received beacon data for sending it to cloud
    public void postBeaconInfo(int list_id, int rssi) {
        if (rssi!=1) { // do not send default value
            long timestamp = new Date().getTime() / 1000;
            String request = String.format("{\"timestamp\": %d, \"signal\": %d}", timestamp, rssi);
            new NetworkConnection().execute("F", docs + "/" + macList[list_id], request);
        }
    }

    // Clears the 'shutdown' container, allowing the processing
    // script to be launched afterwards
    public void clear() {
        new NetworkConnection().execute("F", docs + "/shutdown/truncate", "{}");
        Log.i(TAG, "Cleared shutdown flag");
    }

    // Creates an entry in the 'shutdown' document container,
    // thereby causing the cloud script to finish
    public void shutdown() {
        new NetworkConnection().execute("F", docs + "/shutdown", "{\"raised\": 1}");
        Log.i(TAG, "Shutting down cloud script");
    }
}
