package com.iotssc.potatomap;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends Activity implements OnMapReadyCallback {

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 456;
    static final String TAG = "MainActivity";
    final int REQUEST_ENABLE_BT = 1;
    final boolean verboseDebug = false;
    BluetoothConnector bleConnector;
    static NetworkQueries networkQueries;
    PositionPullingThread ppThread;
    ScriptLaunchingThread slThread;

    AlertDialog mSelectionDialog;
    DevicesAdapter mDevicesAdapter;

    static Marker positionMarker;
    GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        networkQueries = new NetworkQueries();
        networkQueries.shutdown();
        ppThread = new PositionPullingThread();
        slThread = new ScriptLaunchingThread();

        bleConnector = new BluetoothConnector(this, this, verboseDebug);
        mDevicesAdapter = new DevicesAdapter(LayoutInflater.from(getApplicationContext()));

        AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder.setTitle(getString(R.string.BLEdeviceListTitle));

        builder.setAdapter(mDevicesAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                bleConnector.finishScanning();
                BluetoothDevice device = (BluetoothDevice) mDevicesAdapter.getItem(i);
                bleConnector.openConnection(device);
            }
        });

        builder.setNegativeButton(R.string.cancel, null);
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                bleConnector.finishScanning();
            }
        });

        //get title text
        String titleText = getString(R.string.BLEdeviceListTitle);
        // Initialize a new foreground color span instance
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);

        // Initialize a new spannable string builder instance
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(titleText);

        // Apply the text color span
        ssBuilder.setSpan(foregroundColorSpan, 0, titleText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Set the alert dialog title using spannable string builder
        builder.setTitle(ssBuilder);

        mSelectionDialog = builder.create();
        bleConnector.configureBluetoothAdapter();

        setContentView(R.layout.activity_bluetooth_conn);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        askForBLEPermission();
    }

    void askForBLEPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Cannot proceed with app operation, requires permissions to be granted",
                            Toast.LENGTH_LONG).show();
                    askForBLEPermission();
                }
            }
        }
    }

    public void onConnectClick(View view) {

        if (bleConnector.isBluetoothAdapterReady()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

        } else {

            if (!bleConnector.isConnected()) {
                networkQueries.clear();
                bleConnector.beginScanning();
                mSelectionDialog.show();
                //set button colour
                Button button = mSelectionDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                button.setTextColor(Color.BLACK);

            } else {
                bleConnector.disconnect();
            }
        }

    }

    void onConnectionStateChange(int status, final int newState) {

        String newButtonText = getString(R.string.connecting);

        if (newState == BluetoothProfile.STATE_CONNECTED) {
            Intent intentAction = new Intent(bleConnector.ACTION_GATT_CONNECTED);
            sendBroadcast(intentAction);
            slThread.start();
            ppThread.start();

            newButtonText = getString(R.string.disconnect);

        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            Log.i(TAG, "Disconnected from GATT server");
            Intent intentAction = new Intent(bleConnector.ACTION_GATT_DISCONNECTED);
            sendBroadcast(intentAction);
            networkQueries.shutdown();
            ppThread.shutdown();

            newButtonText = getString(R.string.connect);
        }


        final String finalNewButtonText = newButtonText;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button button = findViewById(R.id.connectionButton);
                button.setText(finalNewButtonText);
            }
        });
    }


    void recordBLEDevice(final BluetoothDevice bluetoothDevice) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDevicesAdapter.add(bluetoothDevice);
                mDevicesAdapter.notifyDataSetChanged();
            }
        });
    }

    static void moveTo(String resultEntry) {
        double latitude = 0, longitude = 0;
        String accuracy = "";
        try {
            JSONArray documents = new JSONArray(resultEntry);
            JSONObject latestDoc = documents.getJSONObject(documents.length() - 1);
            accuracy = latestDoc.getString("acc");
            JSONArray coords = new JSONArray(latestDoc.getString("pos"));
            latitude = coords.getDouble(0);
            longitude = coords.getDouble(1);
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }
        LatLng latLng = new LatLng(latitude, longitude);

        if (accuracy.equals("low")) {
            positionMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        } else if (accuracy.equals("medium")) {
            positionMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
        } else /* if (accuracy.equals("high")) */ {
            positionMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        }
        positionMarker.setPosition(latLng);
        Log.i(TAG, "Set new position [" +latitude+longitude+ "]");

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        //set marker at appleton
        LatLng appletonPosition = new LatLng(55.94443, -3.18675);
        //keep reference for later use
        positionMarker = mMap.addMarker(new MarkerOptions().
                position(appletonPosition).
                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        //set overlay on the map
        GroundOverlayOptions newarkMap = new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromResource(R.drawable.at_floor5_v4))
                .position(appletonPosition, 52f);
        mMap.addGroundOverlay(newarkMap);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(appletonPosition, 20));

    }

}
