package com.iotssc.potatomap;

/**
 * ScriptLaunchingThread.java
 * Accesses the cloud endpoint script that begins data processing.
 * Contained in a separate thread to not interfere with other cloud requests,
 * as the connection stays active until the script finishes.
 */

import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class ScriptLaunchingThread extends Thread {
    private String TAG = "ScriptLaunchingThread";

    @Override
    public void run() {
        Log.i(TAG, "Cloud script started");
        String host = "http://glenlivet.inf.ed.ac.uk:8080/api/v1/svc/ep/connect";
        String headerAuth = "Bearer 59:f1a0e0e20d7ba1484533a7833a66f7849805e51a";
        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(host);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Authorization", headerAuth);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            Log.i(TAG, "Cloud script finished with " + urlConnection.getResponseCode());

        } catch (IOException e) {
            Log.e(TAG, e.toString());

        } finally {
            urlConnection.disconnect();
            Log.i(TAG, "Cloud connection ended");
        }
    }
}