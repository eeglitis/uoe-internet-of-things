#pragma once

#include "mbed.h"
#include "BLE.h"
#include "BLESender.h"
#include "GattCharacteristic.h"
#include "LSM9DS1.h"

#include <math.h>


/*
*   This class serves as a custom BLE service provider
*   The it provides a BLE characteristic for each IMU reading: accelerometer, gyroscope and magnetometer
*   That allows sending value from each sensor individually
* 
*   DISCLAIMER: this file is a combination of:
*   1. My original code 
*   1. https://os.mbed.com/users/4180_1/code/LSM9DS1_Demo_wCal/
*   2. https://os.mbed.com/users/beanmachine44/code/LSM9DS1/ (with some alteration to the library itself)
*/




//added enum that was lacking in the library
enum fifoMode_type {
    FIFO_OFF = 0,
    FIFO_THS = 1,
    FIFO_CONT_TRIGGER = 3,
    FIFO_OFF_TRIGGER = 4,
    FIFO_CONT = 5
};

class BLEIMUService
{


private:

    //UUID of the custom service
    uint16_t serviceUUID;
    //UUIDs of it characteristics
    uint16_t characteristicUUIDs[3];
    //the characteristics themselves
    GattCharacteristic* characteristics[3];
    //pointer to the BLE stack
    BLE* bluetooth;
    //pointer to the BLE sender
    BLESender* sender;
    //variable signifying if it should react to function calls
    bool paused;
    //3 measurement for each one of the sensors
    float characteristicValues[3][3];
    //scaled bias for read measurements
    float accelerometerBias[3], gyroscopeBias[3];
    //raw bias for the measurements
    int16_t accelerometerBiasRaw[3], gyroscopeBiasRaw[3];
    //pointer to the library for the IMU chip
    LSM9DS1* imu;

public:

    //constructor
    BLEIMUService(BLE* bluetooth, BLESender* sender, uint16_t serviceUUID);
    //method to pause class operation
    void pause();
    //method to resuem class operation
    void resume();
    //call to read out all IMU sensors and queue their sending into the EventQueue
    void updateAllSensorReadings();


private:

    //do the local calibration
    void calibrate();
    //I2C write, extracted from the library to be used in calibration functionality
    void writeI2CByte(uint8_t subAddress, uint8_t data);
    //I2C read, extracted from the library to be used in calibration functionality
    uint8_t readI2CByte(uint8_t subAddress);
    //enable IMU reading accumulation
    void enableFIFO(bool enable);
    //set IMU reading accumulation mode
    void setFIFO(fifoMode_type fifoMode, uint8_t fifoThs);
    //generic queuing of an IMU characteristic update into the EventQueue
    void updateCharacteristic(uint32_t index, float* value);
    //get fresh values from the accelerometer
    void updateAccelerometer(float x, float y, float z);
    //get fresh values from the gyroscope
    void updateGyroscope(float x, float y, float z);
    //get fresh values from the magnetometer
    void updateMagnetometer(float x, float y, float z);
    //scale raw IMU accelerometer reading
    float calculateAccelValue(int16_t accel) ;
    //scale raw IMU gyroscope reading
    float calculateGyroValue(int16_t gyro);
    //scale raw IMU magnetometer sensor reading
    float calculateMagValue(int16_t mag);
};





