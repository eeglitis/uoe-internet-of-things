#include "BLEIMUService.h"


BLEIMUService::BLEIMUService(BLE* bluetooth, BLESender* sender, uint16_t serviceUUID)
{
  
    //record arguments into local variables
    this->serviceUUID = serviceUUID;
    this->bluetooth = bluetooth;
    this->sender = sender;
    this->paused = false;

    //instantiate the IMU
    imu = new LSM9DS1(p29, p28);

    //initialise the IMU sensor BLE characteristics
    for(uint32_t i = 0; i < 3; i++) {
        characteristicUUIDs[i] = i+1;
        characteristics[i] = new GattCharacteristic(
            characteristicUUIDs[i],
            (uint8_t *)&characteristicValues[i],
	    //scaled floats for sensor X,Y,Z in the same characteristic					    
            sizeof(float)*3,
            sizeof(float)*3,
            (uint8_t)
            (GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ |
             GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE |
             GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_NOTIFY));
    }

    //create and add the BLE service
    GattService service(serviceUUID, characteristics, 3);
    bluetooth->gattServer().addService(service);

    //initiate the IMU
    imu->begin(LSM9DS1::G_SCALE_245DPS,
               LSM9DS1::A_SCALE_2G,
               LSM9DS1::M_SCALE_4GS,
               LSM9DS1::G_ODR_952_BW_100,
               LSM9DS1::A_ODR_952,
               LSM9DS1::M_ODR_80);

    //calibrate the gyro an accelerometer
    calibrate();

}

void BLEIMUService::pause()
{
    paused = true;
}
void BLEIMUService::resume()
{
    paused = false;
}

void BLEIMUService::updateAllSensorReadings()
{
    //do not do anything if paused
    if(paused) {
        return;
    }

    //read accelerometer and update value in characteristic
    imu->readAccel();
    updateAccelerometer(imu->ax + accelerometerBias[0], imu->ay + accelerometerBias[1], imu->az + accelerometerBias[2]);

    //read gyroscope and update value in characteristic
    imu->readGyro();
    updateGyroscope(imu->gx + gyroscopeBias[0], imu->gy + gyroscopeBias[1], imu->gz + gyroscopeBias[2]);

    //read magnetometer and update value in characteristic
    imu->readMag();
    updateMagnetometer(imu->mx, imu->my, imu->mz);

}

void BLEIMUService::calibrate()
{
    //track amount of samples
    uint8_t samples = 0;
    int i;
    //reste the sensor bias arrays
    uint8_t accelerometerBiasRawTemp[3] = {0, 0, 0};
    uint8_t gyroscopeBiasRawTemp[3] = {0, 0, 0};
    
    // Turn on FIFO and set threshold to 32 samples
    enableFIFO(true);
    setFIFO(FIFO_THS, 0x1F);
    
    //collect samples
    while (samples < 0x1F) {
        samples = (readI2CByte(FIFO_SRC) & 0x3F); // Read number of stored samples
    }
    
    //accumulate the sensor readings from the FIFO
    for(uint32_t i = 0; i < samples ; i++) {
        // Read the gyro data stored in the FIFO
        imu->readGyro();
        gyroscopeBiasRawTemp[0] += imu->gx;
        gyroscopeBiasRawTemp[1] += imu->gy;
        gyroscopeBiasRawTemp[2] += imu->gz;

        imu->readAccel();
        accelerometerBiasRawTemp[0] += imu->ax;
        accelerometerBiasRawTemp[1] += imu->ay;
        accelerometerBiasRawTemp[2] += imu->az - (int16_t)(1./imu->aRes); 
    }
    
    //calculate average of readings from the sensors in the FIFO
    for (uint32_t i = 0; i < 3; i++) {
        gyroscopeBiasRaw[i] = gyroscopeBiasRawTemp[i] / samples;
        gyroscopeBias[i] = calculateGyroValue(gyroscopeBiasRaw[i]);
        accelerometerBiasRaw[i] = accelerometerBiasRawTemp[i] / samples;
        accelerometerBias[i] = calculateAccelValue(accelerometerBiasRaw[i]);
    }

    //disable IMU FIFO
    enableFIFO(false);
    //reset the FIFO mode
    setFIFO(FIFO_OFF, 0x00);

}


void BLEIMUService::writeI2CByte(uint8_t subAddress, uint8_t data)
{
    char temp_data[2] = {subAddress, data};
    imu->i2c.write(IMU_XGADDDR, temp_data, 2);
}


uint8_t BLEIMUService::readI2CByte(uint8_t subAddress)
{
    char data;
    char temp[2] = {subAddress};

    imu->i2c.write(IMU_XGADDDR, temp, 1);
    //i2c.write(address & 0xFE);
    temp[1] = 0x00;
    imu->i2c.write(IMU_XGADDDR, temp, 1);
    imu->i2c.read(IMU_XGADDDR, &data, 1);
    return data;
}


void BLEIMUService::enableFIFO(bool enable)
{
    uint8_t temp = readI2CByte(CTRL_REG9);
    if (enable) temp |= (1<<1);
    else temp &= ~(1<<1);
    writeI2CByte(CTRL_REG9, temp);
}

void BLEIMUService::setFIFO(fifoMode_type fifoMode, uint8_t fifoThs)
{
    uint8_t threshold = fifoThs <= 0x1F ? fifoThs : 0x1F;
    writeI2CByte(FIFO_CTRL, ((fifoMode & 0x7) << 5) | (threshold & 0x1F));
}

void BLEIMUService::updateCharacteristic(uint32_t index, float* value)
{
    bool valueChanged = false;
    
    //only indicate value change if the value actually changed
    for(uint32_t i = 0; i < 3; i ++) {
        if(value[i] != characteristicValues[index][i]) {
            valueChanged = true;
        }
    }

    //if value did change, record that and send it out
    if(valueChanged) {

        for(uint32_t i = 0; i < 3; i ++) {
            characteristicValues[index][i] = value[i];
        }

        //enqueue the actual sending int othe global EventQueue
        sender->queueSend(characteristics[index],  (uint8_t*) &characteristicValues[index], sizeof(float)*3);
    }

}


void BLEIMUService::updateAccelerometer(float x, float y, float z)
{
    float newX = x, newY = y, newZ = z;
    
    //if it is a fluctuation rather than actual reading, discard the value
    if(std::abs(x) < IMU_ACCEL_SENSITIVITY_THRESHOLD) {
        newX = 0;
    }

    //if it is a fluctuation rather than actual reading, discard the value
    if(std::abs(y) < IMU_ACCEL_SENSITIVITY_THRESHOLD) {
        newY = 0;
    }

    //if it is a fluctuation rather than actual reading, discard the value
    if(std::abs(z) < IMU_ACCEL_SENSITIVITY_THRESHOLD) {
        newZ = 0;
    }

    float newValue[3] = {newX, newY, newZ};
    updateCharacteristic(BLE_ACCELEROMETER_CHAR_UUID-1, newValue);
}

void BLEIMUService::updateGyroscope(float x, float y, float z)
{

    float newX = x, newY = y, newZ = z;

    //if it is a fluctuation rather than actual reading, discard the value
    if(std::abs(x) < IMU_GYRO_SENSITIVITY_THRESHOLD) {
        newX = 0;
    }
    
    //if it is a fluctuation rather than actual reading, discard the value
    if(std::abs(y) < IMU_GYRO_SENSITIVITY_THRESHOLD) {
        newY = 0;
    }
    
    //if it is a fluctuation rather than actual reading, discard the value
    if(std::abs(z) < IMU_GYRO_SENSITIVITY_THRESHOLD) {
        newZ = 0;
    }

    float newValue[3] = {newX, newY, newZ};
    updateCharacteristic(BLE_GYROSCOPE_CHAR_UUID-1, newValue);
}

void BLEIMUService::updateMagnetometer(float x, float y, float z)
{
    float newValue[3] = {x,y,z};
    updateCharacteristic(BLE_MAGNETOMETER_CHAR_UUID-1, newValue);
}

float BLEIMUService::calculateGyroValue(int16_t gyro)
{
    //calculate actual value = raw * scaling
    return imu->gRes * gyro;
}

float BLEIMUService::calculateAccelValue(int16_t accel)
{
    //calculate actual value = raw * scaling
    return imu->aRes * accel;
}

float BLEIMUService::calculateMagValue(int16_t mag)
{
    //calculate actual value = raw * scaling
    return imu->mRes * mag;
}