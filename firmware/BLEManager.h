#pragma once

#include "BLELocalisationConstants.h"
#include "BLEBeaconService.h"

#include "mbed.h"
#include "BLE.h"
#include "GattCharacteristic.h"

/*
 *   This class serves as a manager of the BLE stack
 *   It controls the callbacks executed by the stack
 *   It indicates when BLE stack is active by blinking LED2 when connected to a master
 */

class BLEManager
{

private:
    //pointer to BLE stack
    BLE* bluetooth;
    //pointer to the global EventQueue
    EventQueue* eventQueue;
    //current value of the allocated LED
    uint8_t ledValue;
    //indicator if currently scanning (for beacons)
    bool scanning;
    //pointet to the custom BLEBeaconService
    BLEBeaconService* beaconService;

    //callbacks for the same-named methods of the stack
    void connectionCallback(const Gap::ConnectionCallbackParams_t* params);
    void disconnectionCallback(const Gap::DisconnectionCallbackParams_t* params);
    void scanCallback(const Gap::AdvertisementCallbackParams_t *params);
    void eventsToProcessCallback (BLE::OnEventsToProcessCallbackContext* context);
    //start a BLE scan if not alrady scanning
    void startScan();
    //stop a BLE scan if such is going on
    void stopScan();

public:

    //constructor
    BLEManager(BLE* bluetooth, EventQueue* eventQueue, uint8_t* serviceUUIDs);
    //start advertising this device as a BLE slave
    void startAdvertising();
    //set the BeaconService pointer for this class
    void setBeaconService(BLEBeaconService* beaconService);
    //blink the allocated LED to indicate correct operation
    void blinkLed();

};