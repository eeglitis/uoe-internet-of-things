#include "BLELocalisationSystem.h"


//initialise the system
BLELocalisationSystem localisationSystem;

int main(void)
{

    //start the system
    localisationSystem.start();
    //ensure the program has something more than 1 line of code
    while(true){}
}