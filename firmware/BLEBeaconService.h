#pragma once

#include "BLELocalisationConstants.h"

#include "mbed.h"
#include "BLE.h"
#include "GattCharacteristic.h"
#include "BLESender.h"

/*
*   This class serves as a custom BLE service provider
*   The it provides each BLE Beacon with its own BLE characteristic
*   That allows sending only the newly recorded RSSI for each individual beacon
*/

class BLEBeaconService
{

private:

    //MAC addresses of the beacons in Appleton
    uint8_t BEACON_MAC_ADDRESSES[BLE_BEACON_COUNT][BLEProtocol::ADDR_LEN];
    //UUID of this custom service
    uint16_t serviceUUID;
    //UUIDs of the characteristics that hold RSSI from beacons
    uint16_t characteristicUUIDs[BLE_BEACON_COUNT];
    //pointers to correspondign characteristics
    GattCharacteristic* characteristics[BLE_BEACON_COUNT];
    //to keep track of which beacons have received data recently
    bool beaconActive[BLE_BEACON_COUNT];
    //last received RSSI value
    uint8_t characteristicValues[BLE_BEACON_COUNT];
    //pointert to BLE stack
    BLE* bluetooth;
    //pointer to BLESender
    BLESender* sender;

public:

    //constructor
    BLEBeaconService(BLE* bluetooth, BLESender* sender, uint16_t serviceUUID);
    //used to update corresponding RSSI value
    void updateBeaconInfo(const Gap::AdvertisementCallbackParams_t* params);
    //reset the record of beacon activity to restart tracking it
    void setBeaconsInactive();
    //get count of beacons that have sent data to the device since last setBeaconsInactive() (os since instantiation)
    uint8_t activeBeaconCount();

private:

    //generic method to queue the sending of a new characteristic value into the EventQueue
    void updateCharacteristic(uint32_t index, uint8_t rssi);

};