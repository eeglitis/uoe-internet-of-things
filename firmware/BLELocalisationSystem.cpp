#include "BLELocalisationSystem.h"

BLELocalisationSystem::BLELocalisationSystem()
{
    //get BLE stack instance
    bluetooth = &BLE::Instance();
    //initial LED value
    ledValue = 0;

    //list of BLE service UUIDs
    const uint16_t serviceUUIDs[]  = {
        BLE_BEACON_SERVICE_UUID
#if SYSTEM_USE_IMU == 1
        ,BLE_IMU_SERVICE_UUID
#endif
    };

    //instantiate global EventQueue
    eventQueue = new EventQueue(SYSTEM_EVENT_QUEUE_SIZE);
    //instantiate BLE stack manager
    bleManager = new BLEManager(bluetooth,
                                eventQueue,
                                (uint8_t*) serviceUUIDs);

    //instantiate the BLE sender
    sender = new BLESender(bluetooth, eventQueue);
    
    //instantiate the custom BLE services
    beaconService = new BLEBeaconService(bluetooth, sender, BLE_BEACON_SERVICE_UUID);
#if SYSTEM_USE_IMU == 1
    imuService = new BLEIMUService(bluetooth, sender, BLE_IMU_SERVICE_UUID);
#endif

    //pass the reference to the BLE Beacon Service into the manager
    bleManager->setBeaconService(beaconService);

    //blink the LED periodically to indicate firmware alive
    eventQueue->call_every(LED_BLINK_PERIOD, this, &BLELocalisationSystem::blinkLed);
#if SYSTEM_USE_IMU == 1
    //start periodic IMU activity management
    eventQueue->call_every(IMU_TICKER_PERIOD, this, &BLELocalisationSystem::manageIMUData);
#endif
    

}

void BLELocalisationSystem::start()
{
    //start BLE advertising
    bleManager->startAdvertising();
    //global EventQueue starts executing all events in it
    eventQueue->dispatch_forever();
}

void BLELocalisationSystem::blinkLed()
{
    DigitalOut led1(LED1);
    led1 = ((ledValue == 0)? 0 : 1);
    ledValue = (ledValue == 1)? 0 : 1;
}

void BLELocalisationSystem::manageIMUData()
{
    //use static values just to not have global ones
    static bool sendIMU = false;
    static uint8_t imuRound = 0;
    
    //get the active beacon count
    uint8_t activeCount = beaconService->activeBeaconCount();

    //system has has high inertia to turning on IMU data sending
    if(imuRound >= IMU_TICKER_INACTIVITY_CHECK_PERIOD) {
        if(activeCount < MIN_ACTIVE_BEACON_COUNT_REQ) {
            //start sending IMU data if below that value
            sendIMU = true;
        }
        
        //set beacons indicators to inactive
        beaconService->setBeaconsInactive();
        //reset the round
        imuRound = 0;
    }
    
    //going back to using only BLE beacons much faster
    else if(activeCount >= MIN_ACTIVE_BEACON_COUNT_REQ) {
        sendIMU = false;
    }

    //if using IMU data, send IMU data
    if(sendIMU) {
        imuService->updateAllSensorReadings();
    }

    //indicate an imu ticker ticked for a round
    imuRound++;
}

