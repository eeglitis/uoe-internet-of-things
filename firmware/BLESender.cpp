#include "BLESender.h"

void BLESender::send(GattAttribute::Handle_t handle, uint8_t* data, uint32_t dataSize)
{
    //physically send the BLE characteristic value
    bluetooth->gattServer().write(handle, data, dataSize);
}


BLESender::BLESender(BLE* bluetooth, EventQueue* eventQueue)
{
    //record values into local values
    this->bluetooth = bluetooth;
    this->eventQueue = eventQueue;
}

void BLESender::queueSend(GattCharacteristic* characteristic, uint8_t* data, uint32_t dataSize)
{
    //disable interrupts
    CriticalSectionLock::enable();

    //get handle to characteristic value 
    GattAttribute::Handle_t handle = characteristic->getValueHandle();
    //issue the send to the global EventQueue
    eventQueue->call(this, &BLESender::send, handle, data, dataSize);

    //enable interrupts
    CriticalSectionLock::disable();

}


