#include "BLEManager.h"

BLEManager::BLEManager(BLE* bluetooth,
                       EventQueue* eventQueue,
                       uint8_t* serviceUUIDs)
{

    //record values into class fields
    this->bluetooth = bluetooth;
    this->eventQueue = eventQueue;

    //initially not scanning
    scanning = false;
    //initially LED is off
    ledValue = 0;

    //initialise BLE stack
    bluetooth->init();
    //reset BLE stack
    bluetooth->gap().reset();

    // Setup the connection and disconnection callbacks
    bluetooth->gap().onConnection(this,  &BLEManager::connectionCallback);
    bluetooth->gap().onDisconnection(this, &BLEManager::disconnectionCallback);
    bluetooth->onEventsToProcess( {this, &BLEManager::eventsToProcessCallback});

    //advertise as a general BLE discoverable
    bluetooth->gap().accumulateAdvertisingPayload(
        GapAdvertisingData::BREDR_NOT_SUPPORTED |
        GapAdvertisingData::LE_GENERAL_DISCOVERABLE);

    //include the custom BLE services into the advertisement
    bluetooth->gap().accumulateAdvertisingPayload(
        GapAdvertisingData::COMPLETE_LIST_16BIT_SERVICE_IDS,
        (uint8_t *)serviceUUIDs, sizeof(serviceUUIDs));

    //advertise pre-defined device name
    bluetooth->gap().accumulateAdvertisingPayload(
        GapAdvertisingData::COMPLETE_LOCAL_NAME,
        (uint8_t *) BLE_DEVICE_NAME, sizeof(BLE_DEVICE_NAME));

    //Set the advertising type
    bluetooth->gap().setAdvertisingType(GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED);
    //Set the advertising interval
    bluetooth->gap().setAdvertisingInterval(100);


}

void BLEManager::scanCallback(const Gap::AdvertisementCallbackParams_t *params)
{
    //on receipt of advertisement data, try to see if it is a BLE beacon
    beaconService->updateBeaconInfo(params);
}

void BLEManager::connectionCallback(const Gap::ConnectionCallbackParams_t* params)
{
    // When a connection is established we need to stop advertising
    bluetooth->gap().stopAdvertising();
    //begin scanning for other devices advertising
    bluetooth->gap().setScanParams(GATT_SCAN_INTERVAL, GATT_SCAN_WINDOW, GATT_SCAN_TIMEOUT);
    //just switch the LED
    blinkLed();
    //start scanning
    startScan();
}


void BLEManager::disconnectionCallback(const Gap::DisconnectionCallbackParams_t* params)
{
    //just switch the LED
    blinkLed();
    //stop scanning
    stopScan();
    // When a client device disconnects we need to start advertising again
    bluetooth->gap().startAdvertising();
}

void BLEManager::eventsToProcessCallback (BLE::OnEventsToProcessCallbackContext* context)
{
    //simply process the event when it arrives, well rather add it into the global EventQueue
    eventQueue->call(bluetooth, &BLE::processEvents);
}


void BLEManager::startScan()
{

    //if already scanning, do nothing
    if(scanning) {
        return;
    }
    
    //indicate scanning active
    scanning = true;
    //begin scanning
    bluetooth->gap().startScan(this, &BLEManager::scanCallback);
}

void BLEManager::stopScan()
{
    //if not scanning, do nothing
    if(!scanning) {
        return;
    }
   
    //indicate scanning disabled
    scanning = false;
    //stop scanning
    bluetooth->gap().stopScan();
}

void BLEManager::startAdvertising()
{
    //Start advertising
    bluetooth->gap().startAdvertising();
}

void BLEManager::setBeaconService(BLEBeaconService* beaconService)
{
    //record BeaconService pointer into class instance
    this->beaconService = beaconService;
}

void BLEManager::blinkLed()
{
    DigitalOut led2(LED2);
    led2 = ((ledValue == 0)? 0 : 1);
    ledValue = (ledValue == 1)? 0 : 1;
}


