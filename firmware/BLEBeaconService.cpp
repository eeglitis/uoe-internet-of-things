#include "BLEBeaconService.h"

BLEBeaconService::BLEBeaconService(BLE* bluetooth, BLESender* sender, uint16_t serviceUUID)
{

    //record values into class instance
    this->serviceUUID = serviceUUID;
    this->bluetooth = bluetooth;
    this->sender = sender;


    //use a proxy for storing the values
    uint8_t temp[BLE_BEACON_COUNT][BLEProtocol::ADDR_LEN] = {
        {0xed, 0x23, 0xc0, 0xd8, 0x75, 0xcd},
        {0xe7, 0x31, 0x1a, 0x8e, 0xb6, 0xd7},
        {0xc7, 0xbc, 0x91, 0x9b, 0x2d, 0x17},
        {0xec, 0x75, 0xa5, 0xed, 0x88, 0x51},
        {0xfe, 0x12, 0xde, 0xf2, 0xc9, 0x43},
        {0xc0, 0x3b, 0x5c, 0xfa, 0x00, 0xb8},
        {0xe0, 0xb8, 0x3a, 0x2f, 0x80, 0x2a},
        {0xf1, 0x55, 0x76, 0xcb, 0x0c, 0xf8},
        {0xf1, 0x7f, 0xb1, 0x78, 0xea, 0x3d},
        {0xfd, 0x81, 0x85, 0x98, 0x88, 0x62}
    };

    //transfer data from the proxy into class array
    for(uint8_t a = 0; a < BLE_BEACON_COUNT; a++) {
        for(uint8_t b = 0; b < BLEProtocol::ADDR_LEN; b++) {
            BEACON_MAC_ADDRESSES[a][b] = temp[a][b];
        }
    }


    //initialise the characteristic IDs
    for(uint32_t i = 0; i < BLE_BEACON_COUNT; i++) {

        //uuid reflects beacon position in the list
        characteristicUUIDs[i] = i+1;
        //initially assume beacons active
        beaconActive[i] = true;
        characteristicValues[i] = 0;

        characteristics[i] = new GattCharacteristic(
            characteristicUUIDs[i],
            (uint8_t *)&serviceUUID,
	    //RSSI is an 8bit value
            sizeof(uint8_t),
            sizeof(uint8_t),
            (uint8_t)
            (GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ |
             GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE |
             GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_NOTIFY));
    }

    //create the BLE service
    GattService service(serviceUUID, characteristics, BLE_BEACON_COUNT);
    //add the service into the BLE stack
    bluetooth->gattServer().addService(service);

}



void BLEBeaconService::updateBeaconInfo(const Gap::AdvertisementCallbackParams_t* params)
{

    //arbitrary starting index
    uint8_t beaconIndex = 0;
    //initially no beacon matches
    bool beaconMatched = false;

    //check received advertisement message belongs to any of the BLE beacons
    for(uint8_t beaconSelect = 0; beaconSelect < BLE_BEACON_COUNT; beaconSelect++) {

	//check for MAC match
        bool macMatched = true;
        for(uint8_t byteSelect = 0; byteSelect < BLEProtocol::ADDR_LEN; byteSelect++) {
            if(BEACON_MAC_ADDRESSES[beaconSelect][byteSelect] !=
                    params->peerAddr[BLEProtocol::ADDR_LEN-byteSelect-1]) {
                macMatched = false;
            }
        }

	//if it was a match break out of the loop
        if(macMatched) {
            beaconMatched = true;
            beaconIndex = beaconSelect;
            break;
        }
    }

    //do not proceed if did not find a matching BLE beacon
    if(!beaconMatched) {
        return;
    }

    //get RSSI value
    uint8_t rssi = params->rssi;

    //actually update the value for the corresponding BLE characteristic
    updateCharacteristic(beaconIndex, rssi);
}

void BLEBeaconService::updateCharacteristic(uint32_t index, uint8_t rssi)
{

    //ensure that send to an existing BLE characteristic
    uint8_t actualIndex = index % BLE_BEACON_COUNT;

    //set new value
    characteristicValues[actualIndex] = rssi;

    //indicate beacon was active, it it was not before
    beaconActive[actualIndex] = true;

    //queue the physical sending into the global EventQueue
    sender->queueSend(characteristics[actualIndex],
                      &characteristicValues[actualIndex],
                      sizeof(characteristicValues[actualIndex]));

}

uint8_t BLEBeaconService::activeBeaconCount()
{

    uint8_t activeBeaconCount = 0;
    
    //see if beacon has sent a value recently
    for(uint32_t i = 0; i < BLE_BEACON_COUNT; i++) {

        //count the active beacons
        if(beaconActive[i]) {
            activeBeaconCount++;
        }
    }
    
    //return the amoung of "fresh" beacon values (i.e. active beacons)
    return activeBeaconCount;
}

void BLEBeaconService::setBeaconsInactive()
{
    
    //set all BLE beacons as inactive, so it can be seen when we do receive a fresh value for them
    for(uint32_t i = 0; i < BLE_BEACON_COUNT; i++) {
        //count the active beacons
        beaconActive[i] = false;
    }
}
