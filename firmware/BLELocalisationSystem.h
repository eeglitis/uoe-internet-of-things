#pragma once

#include "BLELocalisationConstants.h"

#include "mbed.h"
#include "BLE.h"
#include "BLEBeaconService.h"
#include "BLEIMUService.h"
#include "BLESender.h"
#include "BLEManager.h"

/*
 *   This class instantiates all of the embedded C++ code for the project
 *   It containts instances of all other classes and acts as the control center for them
 *   It instantiates other classes and links them to each other
 *   It blinks LED1 to indicate correct operation
 */

class BLELocalisationSystem
{

private:

    //value to hold value of LED
    uint8_t ledValue;
    //global queue of events
    EventQueue* eventQueue;
    //bluetooth stack
    BLE* bluetooth;
    //custom beacon RSSI service
    BLEBeaconService* beaconService;
    //custom IMU data service
    BLEIMUService* imuService;
    //accessor class to have atomic addition to the event queue
    BLESender* sender;
    //BLE stack manager
    BLEManager* bleManager;

public:

    //constructor
    BLELocalisationSystem();
    //start operating
    void start();

private:

    //indicate overall system is working
    void blinkLed();
    //get and queue sending of IMU data
    void manageIMUData();

};

