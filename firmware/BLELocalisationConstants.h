//------------------------------------------------------
// BEACON DEFINITIONS
//------------------------------------------------------
//number of beacons we are interested in
#define BLE_BEACON_COUNT 10

//------------------------------------------------------
// IMU DEFINITIONS
//------------------------------------------------------
//IC2 address for the IMU
#define IMU_XGADDDR 0xD6
//sensitivity threshold for the accelerometer
#define IMU_ACCEL_SENSITIVITY_THRESHOLD 0.1f
//sensitivity theshold for the gyroscope
#define IMU_GYRO_SENSITIVITY_THRESHOLD 2.2f
//no theshold for the magnetometer as fluctuations are around a set point

//------------------------------------------------------
// BLE SENDER DEFINITIONS
//------------------------------------------------------
//maximum size of the ble sender queue
#define MAX_BLE_SENDER_QUEUE_SIZE 20
//maximum data size of the packet the BLE sender can transfer
#define MAX_BLE_SENDER_DATA_SIZE (sizeof(float)*3)

//------------------------------------------------------
// BLE SERVICE DEFINITIONS
//------------------------------------------------------
//as defice appears on scans
#define BLE_DEVICE_NAME "Potato Embed"
//uuid of the beacon data service
#define BLE_BEACON_SERVICE_UUID 0x0001
//uuid of the imu data service
#define BLE_IMU_SERVICE_UUID 0x0002
//uuid of the accelrometer characteristic of the imu data service
#define BLE_ACCELEROMETER_CHAR_UUID 0x0001
//uuid of the gyroscope characteristic of the imu data service
#define BLE_GYROSCOPE_CHAR_UUID 0x0002
//uuid of the gyroscope characteristic of the imu data service
#define BLE_MAGNETOMETER_CHAR_UUID 0x0003

//------------------------------------------------------
// OVERALL SYSTEM DEFINITIONS
//------------------------------------------------------
//scan window in miliseconds
#define GATT_SCAN_WINDOW 100
//interval between scan windows
#define GATT_SCAN_INTERVAL 200
//timeout of the scan in ms, i.e. time it stops
#define GATT_SCAN_TIMEOUT 0
//when active beacon count falls beneath this value, send IMU data
#define MIN_ACTIVE_BEACON_COUNT_REQ 100 //never, as not using IMU data 
//check for active beacons IMU this often
#define IMU_TICKER_PERIOD 100
//how many ticket tickes to check if sensors are inactive
#define IMU_TICKER_INACTIVITY_CHECK_PERIOD 30
//event queue size, x10 to account for any extra events or event size I did not account for
#define SYSTEM_EVENT_QUEUE_SIZE (MAX_BLE_SENDER_QUEUE_SIZE*MAX_BLE_SENDER_DATA_SIZE*10)
//how often should the LEDs blink while device operational
#define LED_BLINK_PERIOD 500
//to use IMU or not, currently not used for localisation
#define SYSTEM_USE_IMU 0
