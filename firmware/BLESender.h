#pragma once

#include "mbed.h"
#include "BLE.h"
#include "GattCharacteristic.h"
#include "CriticalSectionLock.h"

#include "BLELocalisationConstants.h"


/*
 *   This class serves as a BLE send serialiser
 */

class BLESender
{

private:

    //pointer to BLE stack
    BLE* bluetooth;
    //pointer to global EventQueue
    EventQueue* eventQueue;
    //BLE message send
    void send(GattAttribute::Handle_t handle, uint8_t* data, uint32_t dataSize);

public:

    //constructor
    BLESender(BLE* bluetooth, EventQueue* eventQueue);
    //enqueue a BLE send for a characteristic into the global EventQueue
    void queueSend(GattCharacteristic* characteristic, uint8_t* data, uint32_t dataSize);

};
